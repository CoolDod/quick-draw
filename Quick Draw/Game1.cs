﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System;

namespace Quick_Draw
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Text alignment enum
        enum Align
        {
            Top,
            Middle,
            Bottom,
            Left,
            Centre,
            Right
        }
        
        // Game state enum
        enum GameState
        {
            Notstarted,
            Started,
            Lose,
            Win
        }

        // Timer state enum
        enum TimerState
        {
            Up,
            Stopped,
            Down
        }

        // Sprite struct
        struct Sprite
        {
            public Texture2D image; // The sprites texture
            public Rectangle rect,  // The sprites rectangle
                bbox;               // The sprites bounding box
            public Vector2 origin;  // The origin point of the sprite
            public Color colour;    // The blend colour of the sprite

            // Contructor 1
            public Sprite(Texture2D img, Point pos)
            {
                // Set variables
                image = img;
                rect = new Rectangle(pos, new Point(img.Width, img.Height));
                origin = Vector2.Zero;
                colour = Color.White;
                bbox = new Rectangle(pos.X - (int)origin.X, pos.Y - (int)origin.Y, rect.Width, rect.Height);
            }

            // Constructor 2
            public Sprite (Texture2D img, Point pos, Point size, Vector2 originPoint, Color col)
            {
                // Set variables
                image = img;
                rect = new Rectangle(pos, size);
                origin = originPoint;
                colour = col;
                bbox = new Rectangle(pos.X - (int)origin.X, pos.Y - (int)origin.Y, rect.Width, rect.Height);
            }
        }

        // Text struct
        struct Text
        {
            public SpriteFont font; // Text font
            public Vector2 pos,     // Text position
                origin,             // Text origin
                size;               // Text width and height
            public string str;      // Text string
            public Align hAlign,    // Horizontal alignment
                vAlign;             // Vertical alignment
            public Color colour;    // Text colour

            // Constructor 1
            public Text(SpriteFont spriteFont, Vector2 position, string textString)
            {
                // Set variables
                font = spriteFont;
                pos = position;
                str = textString;
                origin = Vector2.Zero;
                hAlign = Align.Left;
                vAlign = Align.Top;
                colour = Color.White;

                // Calculate size of text
                size = font.MeasureString(str);
            }

            // Constructor 2
            public Text(SpriteFont spriteFont, Vector2 position, string textString, Color col, Align horizontalAlign, Align verticalAlign)
            {
                // Set variables
                font = spriteFont;
                pos = position;
                str = textString;
                origin = Vector2.Zero;
                hAlign = horizontalAlign;
                vAlign = verticalAlign;
                colour = col;

                // Calculate size of string
                size = font.MeasureString(str);

                // Calculate X of origin
                if (hAlign == Align.Centre)
                    origin.X = size.X / 2;
                else if (hAlign == Align.Right)
                    origin.X = size.X;

                // Calculate Y of origin
                if (vAlign == Align.Centre)
                    origin.Y = size.Y / 2;
                else if (vAlign == Align.Bottom)
                    origin.Y = size.Y;
            }
        }

        // Sprites
        Sprite button;

        // Texts
        Text prompt,
            title;

        // Fonts
        SpriteFont mainFont;

        // Sound Effects
        SoundEffect buttonClick;

        // Colours
        Color backgroundCol = Color.Lerp(Color.DarkGray, Color.Black, 0.7f);

        // Vectors
        Vector2 screenSize, screenCentre;

        // Mouse States
        MouseState mouse,   // The current mouse state
            mousePrev;      // The previous mouse state

        // RNG
        Random rnd = new Random();

        // Game state
        GameState state = GameState.Notstarted;

        // Timer state
        TimerState timerState = TimerState.Stopped;

        //Floats
        float timer = 0f;
        
        // Constants
        const int promptMargin = 15;
        const int titleMargin = 15;
        const string titleString = "Quick Draw";
        const string startPrompt = "Click the button to start.";
        const string getreadyPrompt = "Get ready!";
        const string clickPrompt = "CLICK!";
        const string losePrompt = "Too soon./nClick the button to restart.";
        const string winPrompt = "Well done!/nClick the button to restart.";

        // FUNCTIONS
        // Measure text
        Text measureText(Text text)
        {
            text.size = mainFont.MeasureString(text.str);

            // Calculate X origin
            if (text.hAlign == Align.Centre)
                text.origin.X = text.size.X / 2;
            else if (text.hAlign == Align.Right)
                text.origin.X = text.size.X;
            else
                text.origin.X = 0;

            // Calculate y origin
            if (text.vAlign == Align.Middle)
                text.origin.Y = text.size.Y / 2;
            else if (text.vAlign == Align.Bottom)
                text.origin.Y = text.size.Y;
            else
                text.origin.Y = 0;

            return text;
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            // Get viewport size
            var scWidth = graphics.GraphicsDevice.Viewport.Width;
            var scHeight = graphics.GraphicsDevice.Viewport.Height;

            screenSize = new Vector2(scWidth, scHeight);
            
            // Calculate screen centre point
            screenCentre = screenSize / 2;

            // Show mouse
            IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            // SPRITES
            // Load button sprite
            var buttonTex = Content.Load<Texture2D>("button");
            // Calculate button size
            var buttonSize = new Vector2(buttonTex.Width, buttonTex.Height);
            // Create new button sprite
            button = new Sprite(buttonTex, screenCentre.ToPoint(), buttonSize.ToPoint(), buttonSize / 2, Color.White);

            // SOUNDS
            // Load button click sound
            buttonClick = Content.Load<SoundEffect>("buttonclick");

            // TEXT
            // Load font
            mainFont = Content.Load<SpriteFont>("mainSpriteFont");

            // Create title text
            title = new Text(mainFont, new Vector2(screenCentre.X, titleMargin), titleString, Color.White, Align.Centre, Align.Top);
            // Create prompt text
            prompt = new Text(mainFont, new Vector2(screenCentre.X, screenCentre.Y + button.origin.Y + promptMargin), startPrompt, Color.White, Align.Centre, Align.Top);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // Get mouse state
            MouseState mouse = Mouse.GetState();

            // BUTTON
            // If button is clicked
            if (mouse.LeftButton == ButtonState.Released && mousePrev.LeftButton == ButtonState.Pressed && button.bbox.Contains(mouse.X, mouse.Y))
            {
                // Play button click sound
                buttonClick.Play();

                // If game is not started
                if (state == GameState.Notstarted)
                {
                    // Set game state to started
                    state = GameState.Started;
                    
                    // Change prompt
                    prompt.str = getreadyPrompt;
                    prompt = measureText(prompt);

                    // Set timer to random value
                    timer = rnd.Next(3, 11);
                    timerState = TimerState.Down;
                }
                // If game started
                else if (state == GameState.Started)
                {
                    // If timer is counting down
                    if (timerState == TimerState.Down)
                    {
                        // Change game state
                        state = GameState.Lose;

                        // Set new prompt
                        prompt.str = losePrompt;

                    }

                    timerState = TimerState.Stopped;
                }
            }

            // TIMER
            // If timer is counting down
            if (timerState == TimerState.Down)
            {
                // Update timer
                timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                // If timer hits 0
                if (timer <= 0)
                {
                    // Set timer to 0
                    timer = 0;
                    // Set timer to count up
                    timerState = TimerState.Up;

                    // Set new prompt
                    prompt.str = clickPrompt;
                    prompt = measureText(prompt);
                }
            }
            // If timer is counting up
            else if (timerState == TimerState.Up)
                // Update timer
                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Store mouse state
            mousePrev = mouse;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(backgroundCol);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            // Draw button
            spriteBatch.Draw(button.image, button.rect, null, button.colour, 0, button.origin, SpriteEffects.None, 0);

            // Draw title
            spriteBatch.DrawString(title.font, title.str, title.pos, title.colour, 0, title.origin, 1.3f, SpriteEffects.None, 0);
            // Draw prompt
            spriteBatch.DrawString(prompt.font, prompt.str, prompt.pos, prompt.colour, 0, prompt.origin, 1, SpriteEffects.None, 0);

            // Draw timer
            var timestr = "Time: ";
            if (timerState == TimerState.Down)
                timestr += Math.Ceiling(timer).ToString();
            else
                timestr += Math.Round(timer, 2).ToString();

            spriteBatch.DrawString(mainFont, timestr, new Vector2(10, 10), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
